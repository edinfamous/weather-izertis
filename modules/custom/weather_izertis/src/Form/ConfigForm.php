<?php

namespace Drupal\weather_izertis\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Save data to use at API.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * Method to get form id.
   *
   * @return string
   *   Return id of form.
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * Method to get confit names.
   *
   * @return string[]
   *   Return the config name.
   */
  protected function getEditableConfigNames() {
    return [
      'weather_izertis.settings',
    ];
  }

  /**
   * Method to build form.
   *
   * @param array $form
   *   Form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Return array with field of form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('weather_izertis.settings');

    $form['city_country_code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('City with Country Code'),
      '#description' => $this->t('City and Country code add in the next structure: <br> cu|Cuba. One by line'),
      '#maxlength' => 255,
      '#size' => 255,
      '#default_value' => $config->get('city_country_code') !== NULL ? $config->get('city_country_code') : 'co|Bogota
       uk|London
       co|Cali
       us|Miami',
      '#weight' => '0',
    ];

    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => 'Endpoint',
      '#description' => 'Endpoint',
      '#default_value' => $config->get('endpoint') !== NULL ? $config->get('endpoint') : 'http://api.openweathermap.org/data/2.5/weather',
    ];

    $form['api_id'] = [
      '#type' => 'textfield',
      '#title' => 'API ID',
      '#description' => 'API id',
      '#default_value' => $config->get('api_id') !== NULL ? $config->get('api_id') : 'f7f726dac4af70b5f506886e2f8c7c70',
    ];

    $form['values_to_show'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Values to show'),
      '#description' => $this->t('Values to show add in the next structure: <br> temp, pressure, humidity, temp_min, temp_max'),
      '#maxlength' => 255,
      '#size' => 255,
      '#default_value' => $config->get('values_to_show') !== NULL ? $config->get('values_to_show') : 'temp, pressure, humidity, temp_min, temp_max',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Method to validate values added in form.
   *
   * @param array $form
   *   Form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @todo Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * Save data.
   *
   * @param array $form
   *   Form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $keys_to_insert = [
      'city_country_code',
      'endpoint',
      'api_id',
      'values_to_show',
    ];
    foreach ($form_state->getValues() as $key => $value) {
      if (in_array($key, $keys_to_insert)) {
        $this->config('weather_izertis.settings')
          ->set($key, $value)
          ->save();
        \Drupal::messenger()->addMessage($key . ' saved');
      }
    }
    parent::submitForm($form, $form_state);
  }

}
