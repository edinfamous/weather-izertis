<?php

namespace Drupal\weather_izertis\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WeatherForm form.
 */
class WeatherForm extends FormBase {

  /**
   * Drupal\weather_izertis\Interfaces\WeatherAPIInterface definition.
   *
   * @var \Drupal\weather_izertis\Interfaces\WeatherAPIInterface
   */
  protected $weatherIzertisWeatherApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->weatherIzertisWeatherApi = $container->get('weather_izertis.weather_api');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'weather_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('weather_izertis.settings');
    $cities_country_code = explode(PHP_EOL, $config->get('city_country_code'));
    $cities_list = [];
    foreach ($cities_country_code as $city_country) {
      $city = (explode('|', $city_country));
      $cities_list[trim($city[1]) . ',' . $city[0]] = trim($city[1]) . ',' . $city[0];
    }

    $form['cities'] = [
      '#type' => 'select',
      '#title' => $this->t('Select city and country code'),
      '#options' => $cities_list,
      '#ajax' => [
        'callback' => '::cityAjaxCallback',
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'edit-output',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Verifying entry...'),
        ],
      ],
      '#validated' => TRUE,
    ];

    // Create a html that will be updated
    // when the user selects an item from the select box above.
    $form['output'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#value' => 'Weather Izertis',
      '#prefix' => '<div id="edit-output">',
      '#suffix' => '</div>',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#disabled' => TRUE,
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      \Drupal::messenger()->addMessage($key . ': ' . ($key === 'text_format' ? $value['value'] : $value));
    }
  }

  /**
   * City change callback.
   *
   * @param array $form
   *   Form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   *
   * @return mixed
   *   Value changed.
   */
  public function cityAjaxCallback(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('weather_izertis.settings');
    $api_id = $config->get('api_id');
    $end_point = $config->get('endpoint');
    $markup = 'nothing selected';
    $data_show = explode(', ', $config->get('values_to_show'));
    $city = $form_state->getValue('cities');
    $markup = $this->weatherIzertisWeatherApi->getWeather($end_point, $city, $api_id, $data_show);

    // Don't forget to wrap your markup in a div with the #edit-output id
    // or the callback won't be able to find this target when it's called
    // more than once.
    $output = "<div id='edit-output'>$markup</div>";

    // Return the HTML markup we built above in a render array.
    return ['#markup' => $output];
  }

}
