<?php

namespace Drupal\weather_izertis\Services;

use Drupal\weather_izertis\Interfaces\WeatherAPIInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Weather API.
 */
class WeatherAPI implements WeatherAPIInterface {

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructor.
   *
   * @param GuzzleHttp\ClientInterface $http_client
   *   HTTP Client.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * Get Weather Data.
   *
   * @param string $endpoint
   *   Endpoint to consume data.
   * @param string $city_country_code
   *   City with country code.
   * @param string $api_id
   *   API ID.
   * @param array $data
   *   Data to return.
   *
   * @return array|int
   *   Return array.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getWeather(string $endpoint, string $city_country_code, string $api_id, array $data) {
    $endpoint = $endpoint . '?q=' . $city_country_code . '&appid=' . $api_id;
    $request = $this->httpClient->request('GET', $endpoint);

    if ($request->getStatusCode() != 200) {
      return $request->getStatusCode();
    }
    $current_weather = json_decode($request->getBody()->getContents());

    $data_to_return = '<ul>';
    foreach ($data as $key => $item) {
      $data_to_return .= '<li>' . $item . ': ' . $current_weather->main->$item . '</li>';
    }
    $data_to_return .= '</ul>';
    return $data_to_return;

  }

}
