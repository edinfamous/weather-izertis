<?php

namespace Drupal\weather_izertis\Interfaces;

/**
 * Weather API Interface.
 */
interface WeatherAPIInterface {

  /**
   * Get Weather Data.
   *
   * @param string $endpoint
   *   Endpoint to consume data.
   * @param string $city_country_code
   *   City name with country code.
   * @param string $api_id
   *   API ID.
   * @param array $data
   *   Data to return.
   */
  public function getWeather(string $endpoint, string $city_country_code, string $api_id, array $data);

}
