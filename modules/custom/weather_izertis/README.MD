Author: Edwin Hernández Cabrera
Date: 24/01/2021

Steps to config:
1. Enable module
2. Go to /admin/config/weather_izertis
3. add Values of API Weather
4. Save data

Validate:
1. Go to /weather-izertis
2. You could see the vaues of cities added in the previous step.
3. You can change the value of API weather and see the response.
